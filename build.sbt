name := "akkaweb"

scalaVersion := "2.11.7"

//cancelable in Global := true

libraryDependencies ++= {
  val akkaVersion          = "2.4.0"
  val akkaStreamVersion    = "2.0.1"

  Seq(
    "com.typesafe.akka" %% "akka-http-core-experimental"          % akkaStreamVersion,
    "com.typesafe.akka" %% "akka-http-experimental"               % akkaStreamVersion,
    "com.typesafe.akka" % "akka-http-spray-json-experimental_2.11" % akkaStreamVersion,
    "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.5"
    
  )
}
