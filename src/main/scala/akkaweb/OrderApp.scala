package akkaweb

import akka.http.scaladsl.client.RequestBuilding
import akka.stream.ActorMaterializer
import akka.actor.ActorSystem
import java.io.IOException
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.RequestContext
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.{Source,Sink, Flow}
import akka.http.scaladsl.server.RouteResult
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.MediaTypes
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.model.ContentType
import akka.http.scaladsl.model.ContentType._
import akka.http.scaladsl.server.Directives._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import akka.http.scaladsl.model.StatusCodes._
import com.typesafe.config.{ ConfigFactory, Config }
import akka.util.Timeout
import scala.concurrent.duration._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.model.headers.RawHeader
import scala.xml.XML
import scala.util.{Success,Failure}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import scala.collection.mutable.ArrayBuffer

case class OmsOrder (ussid:String , tx:String, o:String, sellerid:String, fftype:String, deltype:String, appprice:String, shippedqty:String)

trait Protocols extends DefaultJsonProtocol {
  implicit val omsOrderFormat = jsonFormat8(OmsOrder.apply)
}

object OrderApp extends App with Protocols {
  val configFile = """
    |akka {
    |  event-handlers = ["akka.event.Logging$DefaultLogger"]
    |  loglevel = "DEBUG"
    |  actor {
    |    provider = "akka.actor.LocalActorRefProvider"
    |  }
    |  log-dead-letters-during-shutdown = false
    |  log-dead-letters = 0
    |}
    |proxy {
    |   service {
    |     name = "ProxySystem"
    |     http.interface = proxy.tcs.com
    |     http.port = 80
    |   }
    |}""".stripMargin

  val config = ConfigFactory.parseString( configFile )

  val interface = config.getString( "proxy.service.http.interface" )
  val systemName = config.getString( "proxy.service.name" )
  val port = config.getInt( "proxy.service.http.port" )
  
 implicit def system = ActorSystem("akka-http",config)
  implicit val materializer = ActorMaterializer()
  implicit val askTimeout: Timeout = 500.millis

  //lazy val commerceOrderSearchFlow: Flow[HttpRequest, HttpResponse, Any] = Http().outgoingConnection("e2e.tataunistore.com", 80)
  //lazy val commerceOrderSearchFlow: Flow[HttpRequest, HttpResponse, Any] = Http().outgoingConnection("localhost", 9001)
  lazy val commerceOrderSearchFlow: Flow[HttpRequest, HttpResponse, Any] = Http().outgoingConnection("10.10.73.65", 9001)
  //oms
  //10.10.73.8
  //http://localhost:8080/oms-ext-web/webresources/statuses/orders
  lazy val omsOrderSearchFlow: Flow[HttpRequest, HttpResponse, Any] = Http().outgoingConnection("10.10.73.71", 8080)

  def orderSearchFlow(request: HttpRequest): Future[HttpResponse] = {
    println("Opening connection to " + request.uri.authority.host.address)
    Source.single(request).via(commerceOrderSearchFlow).runWith(Sink.head)
  }
  
  def omsorderSearchFlow(request: HttpRequest): Future[HttpResponse] = {
    println("Opening connection to " + request.uri.authority.host.address)
    Source.single(request).via(omsOrderSearchFlow).runWith(Sink.head)
  }
  //#q=test

//  val authorization = headers.Authorization(BasicHttpCredentials("siteadmin", "ASDF!@#$asdf1234"))
  def orderSearch(code: String): Future[String] = {
    //println(RequestBuilding.Get(s"/?q=$postcode").addHeader(new Authorization(BasicHttpCredentials("siteadmin", "ASDF!@#$asdf1234"))))
    //e2eorderSearch(RequestBuilding.Get(s"/?q=$postcode")).flatMap { response =>
    //e2eorderSearch(RequestBuilding.Get(s"/store/mpl/en/search/?searchCategory=all&text=$postcode").addHeader(new Authorization(BasicHttpCredentials("siteadmin", "ASDF!@#$asdf1234")))).flatMap { response =>
    //order code 143156581
    println(RequestBuilding.Get(s"/marketplacewebservices/v2/mpl/orders1/$code"))
      orderSearchFlow(RequestBuilding.Get(s"/marketplacewebservices/v2/mpl/orders1/$code")).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[String]
        case BadRequest => Future.successful("whoops bad request")
        case _ => Unmarshal(response.entity).to[String].flatMap { entity =>
          val error = s"FAIL - ${response.status}"
          Future.failed(new IOException(error))
        }
      }
    }
  }

  
  
   //def omsOrderSearch(code: String): Future[String] = {
  def omsOrderSearch(code: String): Future[Either[String,List[OmsOrder]]] = {
      //http://localhost:8080/oms-ext-web/webresources/orders/{orderid}
     println(RequestBuilding.Get(s"/oms-ext-web/webresources/statuses/orders/$code"))
      omsorderSearchFlow(RequestBuilding.Get(s"/oms-ext-web/webresources/orders/$code").withHeaders(RawHeader("x-tenantid","single"))).flatMap { response =>
      response.status match {
        
        case OK => {
          Unmarshal(response.entity).to[String].flatMap{ res =>
             //println(res)
             val xmlo = XML.loadString(res)
//             val oid = xmlo \\"orderId"
//             oid.theSeq.head.text
//             Future.successful(OmsOrder(oid.theSeq.head.text)).map(Right(_))
             
               var buf = ArrayBuffer[OmsOrder]()
               val ol = xmlo \ "orderLines"
        ol.map {n =>
          val sellerid = n \\ "sellerId"
          val fftype = n \\ "fulfillmentType"
          val deltype = n \\ "deliveryType"
          val appprice=  n \\ "approtionedPrice"
          val shippedqty = n \\ "shippedQuantity"
          val ussid = n \\ "ussId"
          val txstatus = n \\ "transactionIdStatus"
          val soid = n \ "sellerOrderId"
          buf += OmsOrder(ussid.text, txstatus.text, soid.text, sellerid.text, fftype.text, deltype.text, appprice.text, shippedqty.text)
       }
  
             buf.toList.foreach(println)
             Future.successful(buf.toList).map(Right(_))
        
   //println(buf.toList)
   
          }
          
          /*val str = Unmarshal(response.entity).to[String]
          val ordercode = str onComplete {
            case Success(st) => {
              val xmlobj = XML.loadString(st)
              val c = xmlobj \\ "orderId"
              println(c.text)
            }
            case Failure(ex) => "not found" 
          }*/
         
          //str
          // Future.successful("ordercode")
          //ordercode
        } 
        case BadRequest => Future.successful(Left("whoops bad request"))
        case _ => Unmarshal(response.entity).to[String].flatMap { entity =>
          val error = s"FAIL - ${response.status}"
          
          Future.failed(new IOException(error))
        }
      }
    }
  }

 /* val routes ={
    pathPrefix("user") {
      (get & path(Segment)){id=>
        complete {
          fetchUser(id).map[ToResponseMarshallable] {
            case Right(userInfo) => userInfo
            case Left(errorMessage) => BadRequest -> errorMessage
          }
        }
      }
    }
  }*/


  def route: RequestContext => Future[RouteResult] = path(""){
    get {
       complete("hello world")
    }
  }~
  path("corder" / Segment){ code =>
    get{
      println(s"Commerce order no $code")
      complete(orderSearch(code).map[HttpResponse](body => HttpResponse(entity = HttpEntity(ContentType(MediaTypes.`application/json`), body))))
    }

  }~
  path("omsorder" / Segment){ code =>
    get{
      println(s"Oms order no $code")
     // complete(omsOrderSearch(code).map[HttpResponse](body => HttpResponse(entity = HttpEntity(ContentType(MediaTypes.`application/json`), body))))
      complete{
          val res = omsOrderSearch(code)
            res.map[ToResponseMarshallable] {
              //case (Right(value)) => value.map {x =>  OmsOrder(x.ussid, x.tx, x.o) }
              case (Right(value)) => value
              case (Left(errorMessage)) => BadRequest -> errorMessage
             // case _ => BadRequest -> errorMessage
            }
      }
    }

  }
  
  val serverBinding = Http().bindAndHandle(interface = "0.0.0.0", port = 8080, handler = route)
}

