package akkaweb

import scala.xml.XML
import scala.io.Source
import java.io.File
import scala.xml.NodeSeq
import scala.collection.mutable.ArrayBuffer

case class omsorderex (ussid:String , tx:String, o:String)

object xmltest extends App {
  
  val xmlstr= "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ns2:order>    <properties/>   <currencyCode>INR</currencyCode>  <emailid>pbkaran@yahoo.com</emailid>    <firstName>Prabakaran</firstName>    <issueDate>2015-12-29T13:58:04Z</issueDate>    <scheduledShippingDate>2015-12-31T13:58:04Z</scheduledShippingDate>    <lastName>Pandurangan</lastName>    <orderId>100003632</orderId>  </ns2:order>"
  
  //println(xmlstr)
    val xmlobj = XML.loadFile(new File("d:/100003630.json"))
    val c = xmlobj \ "orderId"
    c.foreach(println)
    println(c.text)
    val ol = xmlobj \ "orderLines"
    var buf = ArrayBuffer[omsorderex]()
    ol.map {n =>
      val ussid = n \\ "ussId"
      val txstatus = n \\ "transactionIdStatus"
      val soid = n \ "sellerOrderId"
      //println(ussid.text)
      buf += omsorderex(ussid.text, txstatus.text, soid.text)
     }
  
   buf.foreach(println)
   println(buf.toList)
   println(buf.groupBy(_.o))
   // println((ol \ "ussId").map {_.text}.foreach(println))
    val ols = ol \ "transactionIdStatus"
    println(ols.text)
    ols.map(_.text).foreach(println)
    val p = for {
      txstatus <- ol \ "transactionIdStatus"
      soid <- ol \ "sellerOrderId"
      ussid <- ol \ "ussId"
     } yield omsorderex(ussid.text, txstatus.text, soid.text)
     
     println(p)
  }